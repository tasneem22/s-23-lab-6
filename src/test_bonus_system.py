from bonus_system import calculateBonuses
import random
import string

amounts = [9999, 10000, 50000, 100000]
bonuses = [1, 1.5, 2, 2.5]
multipliers = [0.5, 0.1, 0.2]
programs = ['Standard', 'Premium','Diamond']

def test_invalid_program():
    for i in list(string.ascii_uppercase):
        for amount in amounts:
            assert calculateBonuses(i,amount) == 0
            assert calculateBonuses(i,amount+1) == 0
            assert calculateBonuses(i,amount-1) == 0

def valid_program(program, amount, multiplier, bonus):
    assert calculateBonuses(program, amount) == (multiplier*bonus)

def test_program():
        for program, multiplier in zip(programs, multipliers):
            valid_program(program, 100001, multiplier, 2.5)
            for amount, bonus in zip(amounts, bonuses):
                valid_program(program, amount, multiplier, bonus)


